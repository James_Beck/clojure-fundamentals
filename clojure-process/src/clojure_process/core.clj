(ns clojure-process.core
  (:require [clojure.core.async
             :as a
             :refer [>! <! >!! <!! go chan buffer close! thread go-loop
                     alts! alts!! timeout]])
  (:gen-class))

(defn single-state-process
  []
  (let [in (chan) out (chan)]
    (go (<! in)
        (>! out "my only state is all good!"))
    [in out]))

(let [[in out] (single-state-process)]
  (>!! in "single state report")
  (<!! out))

(def result (chan (buffer 5)))

(go-loop []
  (println "Result is " (<! result))
  (recur))

(def result1 (chan 10))
(def result2 (chan 10))
(def result3 (chan 10))

(go-loop []
  (let [[v ch] (alts! [result1 result2 result3])]
    (println "Got " v " from " ch)
    (recur)))

(def jmap (java.util.LinkedHashMap. {1 "1" 2 "2"}))
(.get jmap 1)
(.get jmap 2)
(.get jmap 3)
(.get jmap "1")
