(ns james-test.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [cheshire.core :as json]))

(defroutes app-routes
  (GET "/" [] "Hello World")
  (route/not-found "Not Found")
  
  (GET "/user" []
    {:status 200
     :headers {"Content-Type" "application/json; charset=utf-8"}
     :body (json/generate-string {:name "John Doe" :status :logged_in})})
  
  (route/not-found "Not Found"))
 

(def app
  (wrap-defaults app-routes site-defaults))
